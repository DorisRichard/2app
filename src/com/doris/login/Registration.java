package com.doris.login;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Registration extends Activity {
Button Submit;
EditText Name,BirthDate,Course;
RadioButton Gender;
RadioGroup Gender1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		Name=(EditText)findViewById(R.id.editText1);
		BirthDate=(EditText)findViewById(R.id.editText2);
		
		Course=(EditText)findViewById(R.id.editText3);
		
		
		
		Gender1=(RadioGroup)findViewById(R.id.radioGroup1);
		int Female = Gender1.getCheckedRadioButtonId();
		Gender=(RadioButton)findViewById(Female);
		
		Submit=(Button)findViewById(R.id.button3);
		Submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			Intent nextIntent=new Intent(getApplicationContext(),Home.class);
			nextIntent.putExtra("Name", Name.getText().toString());
			nextIntent.putExtra("BirthDate", BirthDate.getText().toString());
			nextIntent.putExtra("Gender", Gender.getText().toString());
			nextIntent.putExtra("Course", Course.getText().toString());
			startActivity(nextIntent);
	
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

}

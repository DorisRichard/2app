package com.doris.login;

import java.util.Date;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Home extends Activity {
Button Regista;
String Name;
String BirthDate;
String Course;
String Gender;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Intent Strings=getIntent();
		Name=Strings.getStringExtra("Name");
		TextView txt=(TextView)findViewById(R.id.textView2);
		txt.setText(Name);
		
		Intent Stringss=getIntent();
		Course=Stringss.getStringExtra("Course");
		TextView txt2=(TextView)findViewById(R.id.textView8);
		txt2.setText(Course);
		
		Intent Strings2=getIntent();
		Gender=Strings2.getStringExtra("Gender");
		TextView txt3=(TextView)findViewById(R.id.textView6);
		txt3.setText(Gender);
		
		
		
		Intent Strings3=getIntent();
		BirthDate=Strings3.getStringExtra("BirthDate");
		TextView txt4=(TextView)findViewById(R.id.textView4);
		txt4.setText(BirthDate);
		
		
		
		
		
		Regista=(Button)findViewById(R.id.button2);
		Regista.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent nextIntent=new Intent(getApplicationContext(),Registration.class);
				startActivity(nextIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

}
